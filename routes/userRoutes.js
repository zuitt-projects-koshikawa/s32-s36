const express = require('express');
const router = express.Router();

// import user controllers
const userControllers = require('../controllers/userControllers');

const auth = require("../auth");
const {verify, verifyAdmin} = auth

// Routes

// User Registration
router.post("/", userControllers.registerUser);

// Retrieve All Users
router.get("/", userControllers.getAllUsers)

// Login
router.post("/login", userControllers.loginUser);

// Retrieve User Details 
router.get("/getUserDetails", verify, userControllers.getUserDetails);

// Check if Email Exist
router.post("/checkEmailExist", userControllers.checkEmailExists);

// Update a Regular User to Admin
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin)

// Update User Details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

// Enroll registered user
router.post("/enroll", verify, userControllers.enroll);

// Retrieve the enrollments of the logged user
router.get ("/getEnrollments",verify, userControllers.getUserEnrollments)


module.exports = router;


