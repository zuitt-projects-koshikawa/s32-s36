

const express = require('express');
const router = express.Router();


// import course controllers

const courseControllers = require('../controllers/courseControllers');

const auth = require("../auth");
// auth is object
// we can destructure  it to obtain its property.
const {verify, verifyAdmin} = auth;

// Routes

// Add Course
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

// Retrieve All Course
router.get('/',courseControllers.getAllCourse);

// Get Single Course
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

// Archive Course Document
router.put('/archive/:id',verify,verifyAdmin, courseControllers.archiveCourse);

// Activate Course Document
router.put('/activate/:id',verify,verifyAdmin, courseControllers.activateCourse);

// Get All Active Courses
router.get('/getActiveCourses', courseControllers.getAllActiveCourses);

// update a course
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

// Inactive Courses
router.get('/getInactiveCourses',verify, verifyAdmin, courseControllers.getInactiveCourses);

// Find Courses by name
router.post('/findCoursesByName', courseControllers.findCoursesByName);

// find course by price
router.post('/findCoursesByPrice', courseControllers.findCoursesByPrice);

//find Get Course enrollee's list by id
router.get('/getEnrollees/:id', verify, verifyAdmin, courseControllers.getEnrollees)

module.exports = router;