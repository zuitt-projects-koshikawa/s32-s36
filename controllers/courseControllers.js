// import course model
const Course = require ("../models/Course")

// const courseControllers = require("../models/Course")

// Add course

module.exports.addCourse = (req, res)=> {

	console.log(req.body);

	let newCourse = new Course ({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive,
		createdOn: req.body.createdOn		
	})

	newCourse.save()
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

module.exports.getAllCourse = (req, res) => {
	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

module.exports.getSingleCourse = (req, res) => {

	console.log(req.params);

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

module.exports.archiveCourse = (req, res) => {

	console.log(req.params.id)

	let courseUpdates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, courseUpdates, {new: true})
	.then(result => res.send(result))
	.catch(err => res.send(err))

};

module.exports.activateCourse = (req, res) => {

	console.log(req.params.id)

	let courseUpdates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, courseUpdates, {new: true})


};

module.exports.getAllActiveCourses = (req, res) =>{

	console.log(req.params)


	Course.find({isActive : true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

// updating course
module.exports.updateCourse = (req, res) => {
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

module.exports.getInactiveCourses = (req, res) => {
	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(err => res.send(err));
}

// find courses by name
module.exports.findCoursesByName = (req, res) => {
	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {
		if(result.length === 0){
			return res.send("No courses found")
		} else {
			return res.send(result)
		}
	})
	.catch (err => res.send(err))
};

// find courses by price

module.exports.findCoursesByPrice = (req, res) => {
	Course.find({price: req.body.price})
	.then (result => {
		if(result.length === 0){
			return res.send("No course found")
		} else {
			return res.send(result);
		}
	})
	.catch (err => res.send(err));
};

module.exports.getEnrollees = (req, res) => {

	console.log(req.params.id)

	Course.findById(req.params.id)
	.then(result => res.send(result.enrollees))	
	.catch(err => res.send(err))
}